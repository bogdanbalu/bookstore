<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\User;
use App\Models\Tenant;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Tenant::factory()->count(3)->create();
        User::factory()->count(10)->create();
        Book::factory()->count(20)->create();
    }
}
