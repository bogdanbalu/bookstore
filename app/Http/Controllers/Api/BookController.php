<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Symfony\Component\HttpFoundation\Response as ResponseStatus;

class BookController extends Controller
{
    public function index()
    {
        return response()->json(Book::where('user_id', auth()->id())->get(),ResponseStatus::HTTP_OK);
    }

    public function show(Book $book)
    {
        return response()->json($book, ResponseStatus::HTTP_OK);
    }
}
