<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBookRequest;
use App\Models\Book;
use Illuminate\Http\Request;
use DB;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('books.index');
    }

    /**
     * Store a new book
     *
     * @param  \App\Http\Requests\Admin\Roles\StoreBookRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreBookRequest $request) {
        Book::create(array_merge([
            'tenant_id' => auth()->user()->tenant_id,
            'user_id' => auth()->user()->id
        ], $request->only('title', 'author', 'release_date')));

        return redirect('books')->with(['status' => true, 'message' => 'The book has been successfully added.']);
    }

    /**
     * List books
     * @param \Illuminate\Http\Request $request
     *
     * @return JSON
     */
    public function list(Request $request)
    {
        $query = Book::select(
            'id',
            'title',
            'author',
            'release_date',
            DB::raw("CASE
                WHEN DATE_ADD(created_at, INTERVAL 2 DAY) <= NOW() THEN 0 ELSE 1 END
            as status")
        );

        $draw = 0;
        $start = 0;
        $perPage = 15;
        $total = $query->count();

        if ($request->has('start')) {
            $start = $request->start;
        }
        if ($request->has('length')) {
            $perPage = $request->length;
        }
        if ($request->has('draw')) {
            $draw = (int)$request->draw;
        }

        $result = $query->skip($start)->take($perPage)->get()->toArray();
        return response()->json([
            'data' => $result,
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total
        ]);
    }

    /**
     * Delete a book
     *
     * @param  \App\Models\Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('/home')->with(['status' => true, 'message' => 'The book has been successfully deleted.']);
    }
}
