@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('message'))
                <div class="alert alert-{{ session('status') ? 'success' : 'warning' }}" role="alert">
                    {{ session('message') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    <table id="tableBooks" class="display table table-hover table-striped table-bordered nowrap" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table = $("#tableBooks").DataTable({
        columns: [
            { title: 'Title', data: 'title' },
            { title: 'Author', data: 'author' },
            { title: 'Release Date', data: 'release_date' },
            { title: 'Options', render: function(data, type, full, meta) {
                if (full.status) {
                    return `<ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a href="/books/delete/${full.id}" class="btn btn-danger" role="button">Delete</a>
                        </li>
                    </ul>`;
                }
                return null;
                }
            },
        ],

        rowCallback: function (row, data) {},
            filter: false,
            info: false,
            ordering: false,
            processing: true,
            retrieve: true
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "books/list",
            type: "get",
        }).done(function (result) {
            table.clear().draw();
            table.rows.add(result.data).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log("failed")
        });
</script>
@endsection
